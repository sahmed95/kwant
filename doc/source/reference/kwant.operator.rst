:mod:`kwant.operator` -- Operators and Observables
==================================================

.. module:: kwant.operator

Observables
-----------
.. autosummary::
   :toctree: generated/

   Density
   Current
   Source
